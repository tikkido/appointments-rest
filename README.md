![appointments-rest](etc/appointments-rest-logo.png)

This project is a simple application for creating, updating, deleting, and viewing a collection of
available appointments with medical providers.  It includes a REST-based interface and database
persistence.

## Table of Contents

  - Building the Project
  - Database Setup
  - Running the Project
  - Using the Swagger Client Interface

## Building the Project

The project can be built in two ways:  as an executable jar, and as a [Docker][] image.

### Building the Project as an Executable Jar

Use the [Gradle][] `build` task to compile and package an executable jar file that contains all its
dependencies.

```console
$ ./gradlew build
```

### Building the Project as a Docker Image

Prerequisites:

  - Build the project as an executable jar

Use [Docker][] to create a local image of this project.

```console
$ docker build -t appointments-rest .
```

## Database Setup

The `appointments-rest` component requires a relational database.  It comes pre-configured for [MariaDB][].

> NOTE: every method of running the project requires this step _except_ using Docker Compose.

To launch a compatible instance of MariaDB in [Docker][], use the following command (*nix example):

```console
$ docker run -it -p 3306:3306 -e MYSQL_DATABASE=appointments -e MYSQL_ROOT_PASSWORD=sesame mariadb:10.4.1
```

> NOTE: MariaDB will continue to run in the current console window.  For the next step, open a new
> console window.  If you prefer to run MariaDB in the background, use `-d` instead of `-it`.

## Running the Project

There are four ways to run this project:

  1. Using the Gradle `bootRun` task
  1. As an executable jar
  1. In Docker (independently)
  1. In Docker using Docker Compose

### Running the Project with the `bootRun` task

Prerequisites:

  - A relational database (see Database Setup)

For development, it is often handy to launch this project using the `spring-boot-gradle-plugin` in [Gradle][].

From the root folder of the project source code (the root folder of the Git repo), use the following
command to run the project in [Gradle][] (*nix example):

```console
$ ./gradlew bootRun
```

### Running the Project as an executable jar

Prerequisites:

  - Build the project as an executable jar
  - A relational database (see Database Setup)

From the root folder of the project source code (the root folder of the Git repo), use the following
command to run the project in [Gradle][] (*nix example):

```console
$ java -jar build/libs/appointments-rest-0.0.1-SNAPSHOT.jar
```

### Running the project in Docker (independently)

Prerequisites:

  - Build the project as an executable jar
  - Build the project as a Docker image
  - A relational database (see Database Setup)

Use the following command to run the project independently in [Docker][] (*nix example):

```console
$ docker run -it -p 8080:8080 appointments-rest
```

### Running the project in Docker with Docker Compose

Prerequisites:

  - Build the project as an executable jar
  - Build the project as a Docker image

Use the following command to run the project and the database together with Docker Compose
(*nix example):

```console
$ docker run -it -p 8080:8080 appointments-rest
```

## Using the Swagger Client Interface

With the project running, visit http://localhost:8080/swagger-ui.html (in a browser) to access the
Swagger client user interface.

### Authorizing the Swagger Client

In the upper-right of the Swagger client interface, click the button labeled _Authorize_.  A modal
dialog will appear asking for an `api_key`.

![Swagger client Authorize button](etc/swagger-client.png)

Paste the following text into the textbox labeled _Value_ and click _Authorize_:

```
Bearer eyJhbGciOiJIUzUxMiJ9.eyJpc3MiOiJodHRwOi8vX0NVUlJFTlRfU0VSVkVSX05BTUVfL3VQb3J0YWwiLCJzdWIiOiJzdHVkZW50IiwiYXVkIjoiaHR0cDovL19DVVJSRU5UX1NFUlZFUl9OQU1FXy91UG9ydGFsIiwiZXhwIjoxNTUxNzU5MTMxLCJpYXQiOjE1NDc1MjU1MzEsImVtYWlsIjoic3RldmVuLnN0dWRlbnRAZXhhbXBsZS5vcmciLCJnaXZlbl9uYW1lIjoiU3RldmVuIiwiZmFtaWx5X25hbWUiOiJTdHVkZW50IiwicGhvbmVfbnVtYmVyIjoiKDU1NSkgNTU1LTU1NTUiLCJuYW1lIjoiU3RldmVuIFN0dWRlbnQiLCJncm91cHMiOlsiU3R1ZGVudHMiXX0.f8LCAOd2OPgvdL-69rihMQI_XFSm0RyEnaT29CUPyfKE12PJv-JNCh1briCEl_oXpMtXo0lo8LtO7bYLbnyd2w
```

Once this authorization token is set, you will be able to interact with the REST APIs provided by
this project using the Swagger client interface.


[mariadb]: https://mariadb.org/
[Docker]: https://www.docker.com/
[Gradle]: https://gradle.org/