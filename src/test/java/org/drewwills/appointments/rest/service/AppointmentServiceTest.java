package org.drewwills.appointments.rest.service;

import org.drewwills.appointments.rest.domain.Appointment;
import org.drewwills.appointments.rest.domain.AppointmentRepository;
import org.drewwills.appointments.rest.domain.AppointmentStatus;
import org.junit.jupiter.api.Test;
import org.junit.platform.runner.JUnitPlatform;
import org.junit.runner.RunWith;

import java.math.BigDecimal;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(JUnitPlatform.class)
public class AppointmentServiceTest {

    private static final String DATE_CREATED_STRING = "2019-01-01";
    private static final Instant DATE_CREATED_INSTANT =
            LocalDate.parse(DATE_CREATED_STRING).atStartOfDay().toInstant(ZoneOffset.UTC);

    private long idCounter = 1L;

    private Appointment oneWeekAgo = createAppointment(idCounter++, Instant.now().minus(Period.ofWeeks(1)));
    private Appointment oneWeekFromNow = createAppointment(idCounter++, Instant.now().plus(Period.ofWeeks(1)));
    private Appointment oneYearFromNow = createAppointment(idCounter++, ZonedDateTime.now(ZoneOffset.UTC).plusYears(1).toInstant());

    private Set<Appointment> appointments =
            Stream.of(oneWeekAgo, oneWeekFromNow, oneYearFromNow).collect(Collectors.toSet());

    private AppointmentRepository appointmentRepository;
    private AppointmentService appointmentService;

    public AppointmentServiceTest() {
        appointmentRepository = mock(AppointmentRepository.class);
        when(appointmentRepository.findByDateTimeBetween(any(Instant.class),any(Instant.class)))
                .thenAnswer(invocation -> {
                    final Instant fromDate = invocation.getArgument(0);
                    final Instant toDate = invocation.getArgument(1);
                    return appointments.stream()
                            .filter(appointment -> appointment.getDateTime().isAfter(fromDate))
                            .filter(appointment -> appointment.getDateTime().isBefore(toDate))
                            .collect(Collectors.toList());
                });
        when(appointmentRepository.findById(any(Long.class)))
                .thenAnswer(invocation -> {
                    return appointments.stream()
                            .filter(appointment -> appointment.getId().equals(invocation.getArgument(0)))
                            .findAny();
                });
        when(appointmentRepository.save(any(Appointment.class)))
                .thenAnswer(invocation -> {
                    final Appointment appointment = invocation.getArgument(0);
                    if (appointment.getId() < 1) {
                        appointment.setId(idCounter++);
                    }
                    return appointment;
                });
        when(appointmentRepository.existsById(any(Long.class)))
                .thenAnswer(invocation -> {
                    return appointments.stream()
                            .filter(appointment -> appointment.getId().equals(invocation.getArgument(0)))
                            .count() > 0;
                });
        appointmentService = new AppointmentService();
        appointmentService.setDefaultSearchPeriodDays(30);
        appointmentService.setRepository(appointmentRepository);
    }

    @Test
    public void searchWithNullParametersTest() {
        final List<Appointment> searchResults = appointmentService.search(null, null);
        assertFalse(searchResults.contains(oneWeekAgo), "Search results SHOULD NOT contain oneWeekAgo");
        assertTrue(searchResults.contains(oneWeekFromNow), "Search results SHOULD contain oneWeekFromNow");
        assertFalse(searchResults.contains(oneYearFromNow), "Search results SHOULD NOT contain oneYearFromNow");
    }

    @Test
    public void searchWithFromDate() {
        final List<Appointment> searchResults = appointmentService.search(LocalDate.now().minus(Period.ofDays(8)), null);
        assertTrue(searchResults.contains(oneWeekAgo), "Search results SHOULD contain oneWeekAgo");
        assertTrue(searchResults.contains(oneWeekFromNow), "Search results SHOULD contain oneWeekFromNow");
        assertFalse(searchResults.contains(oneYearFromNow), "Search results SHOULD NOT contain oneYearFromNow");
    }

    @Test
    public void searchWithToDate() {
        final List<Appointment> searchResults = appointmentService.search(null, LocalDate.now().plus(Period.ofYears(2)));
        assertFalse(searchResults.contains(oneWeekAgo), "Search results SHOULD NOT contain oneWeekAgo");
        assertTrue(searchResults.contains(oneWeekFromNow), "Search results SHOULD contain oneWeekFromNow");
        assertTrue(searchResults.contains(oneYearFromNow), "Search results SHOULD contain oneYearFromNow");
    }

    @Test
    public void searchWithFromDateAndToDate() {
        final List<Appointment> searchResults =
                appointmentService.search(LocalDate.now().minus(Period.ofDays(8)), LocalDate.now().plus(Period.ofYears(2)));
        assertTrue(searchResults.contains(oneWeekAgo), "Search results SHOULD contain oneWeekAgo");
        assertTrue(searchResults.contains(oneWeekFromNow), "Search results SHOULD contain oneWeekFromNow");
        assertTrue(searchResults.contains(oneYearFromNow), "Search results SHOULD contain oneYearFromNow");
    }

    @Test
    public void getAppointmentPresent() {
        final Appointment appointment = appointmentService.getAppointment(1);
        assertTrue(appointment != null, "Appointment with id=1 SHOULD be present");
    }

    @Test
    public void getAppointmentAbsent() {
        final Appointment appointment = appointmentService.getAppointment(7);
        assertTrue(appointment == null, "Appointment with id=7 SHOULD NOT be present");
    }

    @Test
    public void createAppointmentHappyPath() {
        final Appointment appointment = new Appointment();
        appointment.setDateTime(Instant.now().plus(Period.ofDays(2)));
        appointment.setDoctorName("Doctor Zhivago");
        appointment.setDurationMinutes(30);
        appointment.setPrice(new BigDecimal("100.00"));
        assertDoesNotThrow(() -> appointmentService.createAppointment(appointment),
                "AppointmentService.createAppointment() SHOULD succeed");
    }

    @Test
    public void createAppointmentIllegalArgument() {
        final Appointment appointment = new Appointment();
        appointment.setId(1000L); // Bad
        appointment.setDateTime(Instant.now().plus(Period.ofDays(2)));
        appointment.setDoctorName("Doctor Zhivago");
        appointment.setDurationMinutes(30);
        appointment.setPrice(new BigDecimal("100.00"));
        assertThrows(IllegalArgumentException.class,
                () -> appointmentService.createAppointment(appointment),
                "AppointmentService.createAppointment() SHOULD NOT succeed");
    }

    @Test
    public void updateAppointmentHappyPath() {
        oneWeekFromNow.setDoctorName("Dr. Who");
        assertDoesNotThrow(() -> appointmentService.updateAppointment(oneWeekFromNow),
                "AppointmentService.updateAppointment() SHOULD succeed");
    }

    @Test
    public void updateAppointmentIllegalArgument() {
        final Appointment appointment = new Appointment(); // id not set
        appointment.setDateTime(Instant.now().plus(Period.ofDays(2)));
        appointment.setDoctorName("Doctor Zhivago");
        appointment.setDurationMinutes(30);
        appointment.setPrice(new BigDecimal("100.00"));
        assertThrows(IllegalArgumentException.class,
                () -> appointmentService.updateAppointment(appointment),
                "AppointmentService.createAppointment() SHOULD NOT succeed");
    }

    @Test
    public void deleteAppointmentHappyPath() {
        // NB:  does not actually delete from the mock repository
        assertDoesNotThrow(() -> appointmentService.deleteAppointment(1L),
                "AppointmentService.deleteAppointment() SHOULD succeed");
    }

    @Test
    public void deleteAppointmentIllegalArgument() {
        final long missingId = 1000L; // Bad
        assertThrows(IllegalArgumentException.class,
                () -> appointmentService.deleteAppointment(missingId),
                "AppointmentService.deleteAppointment() SHOULD NOT succeed");
    }

    private Appointment createAppointment(long id, Instant dateTime) {
        final Appointment rslt = new Appointment();
        rslt.setId(id);
        rslt.setCreated(DATE_CREATED_INSTANT);
        rslt.setDateTime(dateTime);
        rslt.setDoctorName("Doctor Zhivago");
        rslt.setDurationMinutes(30);
        rslt.setPrice(new BigDecimal("100.00"));
        rslt.setStatus(AppointmentStatus.Available);
        return rslt;
    }

}
