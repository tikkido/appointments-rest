package org.drewwills.appointments.rest.controller;

import org.drewwills.appointments.rest.domain.Appointment;
import org.drewwills.appointments.rest.service.AppointmentService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.CacheControl;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import javax.servlet.http.HttpServletResponse;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@RestController
@CrossOrigin(origins = "${appointments.cors.origins:}")
@RequestMapping(AppointmentsController.API_ROOT)
public class AppointmentsController {

    /**
     * Defines the prefix of all URIs supported by this controller.
     */
    /* package-private */ static final String API_ROOT = "/api/v1/appointments";

    private AppointmentService service;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    @Autowired
    public void setService(AppointmentService service) {
        this.service = service;
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<Appointment> search(
                    @RequestParam(value = "fromDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate fromDate,
                    @RequestParam(value = "toDate", required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate toDate,
                    HttpServletResponse response
            ) {

        setResponseHeadersForGet(response);

        try {
            return service.search(fromDate, toDate, /* TODO: sort parameter */ null);
        } catch (Exception e) {
            logger.warn("Internal server error", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

    }

    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Appointment getAppointmentById(
                    @PathVariable("id") long id,
                    HttpServletResponse response
            ) {

        setResponseHeadersForGet(response);

        Appointment rslt;
        try {
            rslt = service.getAppointment(id);
        } catch (Exception e) {
            logger.warn("Internal server error", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

        if (rslt == null) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, "Appointment not found:  " + id);
        }

        return rslt;

    }

    @PostMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public Appointment createAppointment(@RequestBody Appointment appointment) {
        try {
            return service.createAppointment(appointment);
        } catch (IllegalArgumentException iae) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST, iae.getMessage());
        } catch (Exception e) {
            logger.warn("Internal server error", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    @PutMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Appointment updateAppointment(@PathVariable("id") long id, @RequestBody Appointment appointment) {

        // The 'id' path variable must match the 'Appointment.id' property
        if (!Objects.equals(id, appointment.getId())) {
            throw new ResponseStatusException(HttpStatus.BAD_REQUEST,
                    "The id in the URI does not match the id in the body of the request");
        }

        try {
            return service.updateAppointment(appointment);
        } catch (IllegalArgumentException iae) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, iae.getMessage());
        } catch (Exception e) {
            logger.warn("Internal server error", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }

    }

    @DeleteMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Map<String,Object> deleteAppointmentt(@PathVariable("id") long id) {
        try {
            service.deleteAppointment(id);
            return Collections.singletonMap("success", true);
        } catch (IllegalArgumentException iae) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, iae.getMessage());
        } catch (Exception e) {
            logger.warn("Internal server error", e);
            throw new ResponseStatusException(HttpStatus.INTERNAL_SERVER_ERROR, e.getMessage());
        }
    }

    private void setResponseHeadersForGet(HttpServletResponse response) {
        final CacheControl cacheControl = CacheControl.noStore();
        response.setHeader(HttpHeaders.CACHE_CONTROL, cacheControl.getHeaderValue());
    }

}
