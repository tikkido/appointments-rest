package org.drewwills.appointments.rest.service;

import org.drewwills.appointments.rest.domain.Appointment;
import org.drewwills.appointments.rest.domain.AppointmentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneOffset;
import java.util.Comparator;
import java.util.List;

/**
 * This <code>@Service</code> bean sits between the controller and the repository.  It implements
 * behavior based on business rules (where needed) that must occur when data is accessed or changed.
 * This pattern allows the controller to focus on responding to HTTP requests, and the repository to
 * focus on interacting with the database, and (thereby) makes both of them easier to understand and
 * maintain.
 */
@Service
public class AppointmentService {

    private int defaultSearchPeriodDays;

    private AppointmentRepository repository;

    @Value("${appointments.defaultSearchPeriodDays:30}")
    public void setDefaultSearchPeriodDays(int defaultSearchPeriodDays) {
        this.defaultSearchPeriodDays = defaultSearchPeriodDays;
    }

    @Autowired
    public void setRepository(AppointmentRepository repository) {
        this.repository = repository;
    }

    public List<Appointment> search(LocalDate fromDate, LocalDate toDate) {
        return search(fromDate, toDate, null);
    }

    public List<Appointment> search(LocalDate fromDate, LocalDate toDate, String sortParameter) {

        final Instant start = fromDate != null
                ? fromDate.atStartOfDay().toInstant(ZoneOffset.UTC)
                : Instant.now();

        final Instant end = toDate != null
                ? toDate.atStartOfDay().toInstant(ZoneOffset.UTC)
                : start.plus(Period.ofDays(defaultSearchPeriodDays));

        // Business rule:  'start' must come before 'end'
        if (!start.isBefore(end)) {
            throw new IllegalArgumentException("Argument fromDate must come before toDate");
        }

        final List<Appointment> rslt = repository.findByDateTimeBetween(start, end);

        // Sort Strategy
        Comparator<Appointment> sortStrategy;
        switch (sortParameter != null ? sortParameter : "default") {
            default:
                // The default sortStrategy is by price ascending
                sortStrategy = Comparator.comparing(Appointment::getPrice);
        }
        rslt.sort(sortStrategy);

        return rslt;

    }

    /**
     * Provides the {@link Appointment} with the specified id, or <code>null</code> if none exists.
     */
    public Appointment getAppointment(long id) {
        return repository.findById(id).orElse(null);
    }

    public Appointment createAppointment(Appointment appointment) {

        // It is not possible to create an appointment that already exists
        if (appointment.getId() > 0) {
            throw new IllegalArgumentException("Cannot create appointment that already exists");
        }

        return repository.save(appointment);

    }

    public Appointment updateAppointment(Appointment appointment) {

        // It is not possible to update an appointment that does not exist
        if (!repository.existsById(appointment.getId())) {
            throw new IllegalArgumentException("Cannot update appointment that does not exist");
        }

        return repository.save(appointment);

    }

    public void deleteAppointment(long id) {

        // It is not possible to delete an appointment that does not exist
        if (!repository.existsById(id)) {
            throw new IllegalArgumentException("Cannot delete appointment that does not exist");
        }

        repository.deleteById(id);

    }

}
