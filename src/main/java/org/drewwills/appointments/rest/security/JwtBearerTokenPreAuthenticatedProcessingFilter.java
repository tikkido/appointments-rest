package org.drewwills.appointments.rest.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jws;
import io.jsonwebtoken.Jwts;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

import javax.servlet.http.HttpServletRequest;
import java.util.Collections;

/**
 * A simple pre-authenticate strategy for Spring Security that accepts a signed JWT as a Bearer
 * token in the Authorization header.
 */
public class JwtBearerTokenPreAuthenticatedProcessingFilter extends AbstractPreAuthenticatedProcessingFilter {

    private static final String BEARER_TOKEN_PREFIX = "Bearer ";
    private static final String USER_DETAILS_REQUEST_ATTRIBUTE = "appointment.userDetails";

    @Value("${appointments.signatureKey:CHANGEME}")
    private String signatureKey;

    private final Logger logger = LoggerFactory.getLogger(getClass());

    /**
     * Check the request headers for an acceptable JWT and process the authentication if present.
     */
    @Override
    protected Object getPreAuthenticatedPrincipal(HttpServletRequest request) {

        final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
        if (StringUtils.isBlank(authHeader)
                || !authHeader.startsWith(BEARER_TOKEN_PREFIX)) {
            /*
             * In authenticating the user, this filter has no opinion if either (1) the
             * Authorization header is not set or (2) the value isn't a Bearer token.
             */
            return null;
        }

        final String bearerToken = authHeader.substring(BEARER_TOKEN_PREFIX.length());

        try {
            // Validate & parse the JWT
            final Jws<Claims> claims =
                    Jwts.parser().setSigningKey(signatureKey).parseClaimsJws(bearerToken);

            logger.debug("Found the following pre-authenticated user:  {}", claims.toString());

            final UserDetails rslt = User
                    .builder()
                    .username(claims.getBody().getSubject())
                    .password(bearerToken)
                    .authorities(Collections.emptySet())
                    .build();
            request.setAttribute(USER_DETAILS_REQUEST_ATTRIBUTE, rslt);

            return rslt;
        } catch (Exception e) {
            logger.info("The following Bearer token is unusable:  '{}'", bearerToken);
            logger.debug("Failed to validate and/or parse the specified Bearer token", e);
        }

        return null;

    }

    /**
     * Must return a non-null value for authenticated users.
     */
    @Override
    protected Object getPreAuthenticatedCredentials(HttpServletRequest request) {
        final UserDetails user = (UserDetails) request.getAttribute(USER_DETAILS_REQUEST_ATTRIBUTE);
        return user != null ? user.getPassword() : null;
    }

}
