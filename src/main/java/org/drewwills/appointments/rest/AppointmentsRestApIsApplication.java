package org.drewwills.appointments.rest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AppointmentsRestApIsApplication {

	public static void main(String[] args) {
		SpringApplication.run(AppointmentsRestApIsApplication.class, args);
	}

}

