package org.drewwills.appointments.rest.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.time.format.DateTimeFormatter;

@Configuration
public class DateAndTimeConfiguration {

    @Bean("dateFormatter")
    public DateTimeFormatter dateFormatter() {
        return DateTimeFormatter.ISO_DATE;
    }

    @Bean("dateTimeFormatter")
    public DateTimeFormatter dateTimeFormatter() {
        return DateTimeFormatter.ISO_INSTANT;
    }

}
