package org.drewwills.appointments.rest.config;

import org.drewwills.appointments.rest.security.JwtBearerTokenPreAuthenticatedProcessingFilter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.web.authentication.preauth.AbstractPreAuthenticatedProcessingFilter;

@Configuration
@EnableWebSecurity
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {


    @Override
    protected void configure(HttpSecurity http) throws Exception {

        http

                /*
                 * Use SessionCreationPolicy.STATELESS so that "Spring Security will never create an
                 * HttpSession and it will never use it to obtain the SecurityContext."  This
                 * approach means that every request needs an Bearer token.
                 */
                .sessionManagement()
                .sessionCreationPolicy(SessionCreationPolicy.STATELESS)
                .and()

                /*
                 * Supply our token-based authentication strategy.
                 */
                .addFilter(preAuthenticatedProcessingFilter())

                /*
                 * Define access rules based on Ant-style path matchers.
                 */
                .authorizeRequests()

                /*
                 * Authenticated users may interact with the REST APIs.
                 */
                .antMatchers("/api/**").authenticated()

                /*
                 * The OPTIONS method is allowed for any URI to support CORS.
                 */
                .antMatchers(HttpMethod.OPTIONS, "/**").permitAll()

                /*
                 * The Swagger UI will be accessible to anyone (though API endpoints require a
                 * Bearer token).
                 */
                .antMatchers(HttpMethod.GET,"/swagger-ui.html").permitAll()
                .antMatchers(HttpMethod.GET,"/swagger-resources/**").permitAll()
                .antMatchers(HttpMethod.GET,"/webjars/**").permitAll()
                .antMatchers(HttpMethod.GET,"/v2/api-docs/**").permitAll()

                /*
                 * Anything not covered by these rules is not permitted.
                 */
                .anyRequest().denyAll()
                .and()

                /*
                 * CSRF defense is not required with Bearer token AuthN (instead of Basic or
                 * cookie-based).
                 * See https://security.stackexchange.com/questions/170388/do-i-need-csrf-token-if-im-using-bearer-jwt.
                 */
                .csrf().disable();

    }

    @Bean
    public AuthenticationManager authenticationManager() {
        return authentication -> {
            if (authentication.getPrincipal() != null && authentication.getCredentials() != null) {
                return new UsernamePasswordAuthenticationToken(
                        authentication.getPrincipal(),
                        authentication.getCredentials());
            }
            throw new BadCredentialsException("Credentials cannot be validated");
        };
    }

    @Bean
    public AbstractPreAuthenticatedProcessingFilter preAuthenticatedProcessingFilter() {
        final AbstractPreAuthenticatedProcessingFilter rslt =
                new JwtBearerTokenPreAuthenticatedProcessingFilter();
        rslt.setAuthenticationManager(authenticationManager());
        return rslt;
    }

}
