package org.drewwills.appointments.rest.config;

import org.drewwills.appointments.rest.domain.Appointment;
import org.drewwills.appointments.rest.domain.AppointmentRepository;
import org.drewwills.appointments.rest.domain.AppointmentStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.math.BigDecimal;
import java.time.Instant;
import java.time.Period;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

@Configuration
@ConditionalOnProperty(name = "appointments.useSampleData", havingValue = "true")
public class SampleDataConfiguration {

    private static final Instant CREATED_DATE_TIME = ZonedDateTime.now(ZoneOffset.UTC).minusYears(1).toInstant();

    @Resource(name = "dateTimeFormatter")
    private DateTimeFormatter dateTimeFormatter;

    @Autowired
    private AppointmentRepository repository;

    @PostConstruct
    public void init() {
        repository.save(sampleAppointmentOne());
        repository.save(sampleAppointmentTwo());
        repository.save(sampleAppointmentThree());
    }

    private Appointment sampleAppointmentOne() {
        final Appointment rslt = new Appointment();
        rslt.setCreated(CREATED_DATE_TIME);
        rslt.setDateTime(Instant.now().minus(Period.ofDays(1)));
        rslt.setDoctorName("Doctor Zhivago");
        rslt.setDurationMinutes(30);
        rslt.setPrice(new BigDecimal("100.00"));
        rslt.setStatus(AppointmentStatus.Available);
        return rslt;
    }

    private Appointment sampleAppointmentTwo() {
        final Appointment rslt = new Appointment();
        rslt.setCreated(CREATED_DATE_TIME);
        rslt.setDateTime(Instant.now().plus(Period.ofDays(1)));
        rslt.setDoctorName("Dr. Moreau");
        rslt.setDurationMinutes(15);
        rslt.setPrice(new BigDecimal("75.00"));
        rslt.setStatus(AppointmentStatus.Available);
        return rslt;
    }

    private Appointment sampleAppointmentThree() {
        final Appointment rslt = new Appointment();
        rslt.setCreated(CREATED_DATE_TIME);
        rslt.setDateTime(Instant.now().plus(Period.ofDays(40)));
        rslt.setDoctorName("Dr. Victor Frankenstein");
        rslt.setDurationMinutes(60);
        rslt.setPrice(new BigDecimal("150.00"));
        rslt.setStatus(AppointmentStatus.Available);
        return rslt;
    }

}
