package org.drewwills.appointments.rest.config;

import org.drewwills.appointments.rest.quartz.AppointmentCreatingJob;
import org.quartz.JobBuilder;
import org.quartz.JobDetail;
import org.quartz.SimpleScheduleBuilder;
import org.quartz.Trigger;
import org.quartz.TriggerBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * This <code>@Configuration</code> bean is defines Quartz jobs responsible for creating new
 * appointments at random intervals
 */
@Configuration
public class QuartzConfiguration {

    @Bean
    public JobDetail appointmentCreatingJobDetail() {
        return JobBuilder.newJob(AppointmentCreatingJob.class)
                .withIdentity("Appointment_Creating_Job_Detail")
                .withDescription("Creates new appointments at a random interval")
                .storeDurably(true)
                .build();
    }

    @Bean
    public Trigger appointmentCreatingTrigger() {
        return TriggerBuilder.newTrigger()
                .forJob(appointmentCreatingJobDetail())
                .withIdentity("Appointment_Creating_Trigger")
                .withDescription("Trigger for creating new appointments at a random interval")
                .withSchedule(SimpleScheduleBuilder.simpleSchedule().withIntervalInMinutes(1).withRepeatCount(100))
                .build();
    }

}
