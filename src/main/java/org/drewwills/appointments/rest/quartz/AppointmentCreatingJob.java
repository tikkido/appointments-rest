package org.drewwills.appointments.rest.quartz;

import org.drewwills.appointments.rest.domain.Appointment;
import org.drewwills.appointments.rest.service.AppointmentService;
import org.quartz.Job;
import org.quartz.JobExecutionContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import java.math.BigDecimal;
import java.time.Duration;
import java.time.Instant;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Benefits from Dependency Injection by means of the {@link AutowiringSpringBeanJobFactory}.
 */
public class AppointmentCreatingJob implements Job {

    private final Logger logger = LoggerFactory.getLogger(getClass());

    private final List<String> doctorNames = Stream.of(
                    "Dr. Who",
                    "Doctor Zhivago",
                    "Dr. Moreau",
                    "Dr. Victor Frankenstein"
            ).collect(Collectors.toList());

    private AppointmentService appointmentService;

    @Autowired
    public void setAppointmentService(AppointmentService appointmentService) {
        this.appointmentService = appointmentService;
    }

    @Override
    public void execute(JobExecutionContext context) {

        final String randomDoctorName = doctorNames.get(
                ThreadLocalRandom.current().nextInt(0, doctorNames.size()));
        final Instant randomDateTime =
                Instant.now().plus(Duration.ofHours(
                        ThreadLocalRandom.current().nextInt(1, 30 * 24)
                ));

        final Appointment appointment = new Appointment();
        appointment.setDoctorName(randomDoctorName);
        appointment.setDateTime(randomDateTime);
        appointment.setDurationMinutes(30);
        appointment.setPrice(new BigDecimal("100.00"));

        logger.debug("Creating new appointment:  {}", appointment);

        appointmentService.createAppointment(appointment);

    }

}
