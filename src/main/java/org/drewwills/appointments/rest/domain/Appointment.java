package org.drewwills.appointments.rest.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.Instant;

@Entity
public class Appointment implements Serializable {

    /**
     * Unique, database-assigned identifier of the appointment.
     */
    @Id
    @GeneratedValue
    private Long id = -1L;

    /**
     * Moment when the appointment was initially created (not when it was booked).
     */
    @Column(name = "created_at", nullable = false)
    private Instant created = Instant.now();

    /**
     * Date and time when the appointment begins.
     */
    @Column(name = "appointment_date", nullable = false)
    private Instant dateTime;

    /**
     * Number of minutes the appointment lasts.
     */
    @Column(name = "appointment_duration", nullable = false)
    private int durationMinutes;

    /**
     * Name of the doctor who will meet with the patient in the appointment.
     */
    @Column(name = "name_of_doctor", nullable = false)
    private String doctorName;

    /**
     * Whether the appointment is <code>Available</code> or <code>Booked</code>.
     */
    @Column(name = "status", nullable = false)
    @Enumerated(EnumType.STRING)
    private AppointmentStatus status = AppointmentStatus.Available;

    /**
     * The amount paid by the patient (or on his behalf) for the appointment.
     */
    @Column(name = "price", precision = 7, scale = 2, nullable = false)
    private BigDecimal price;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Instant getCreated() {
        return created;
    }

    public void setCreated(Instant created) {
        this.created = created;
    }

    public Instant getDateTime() {
        return dateTime;
    }

    public void setDateTime(Instant DateTime) {
        this.dateTime = DateTime;
    }

    public int getDurationMinutes() {
        return durationMinutes;
    }

    public void setDurationMinutes(int durationMinutes) {
        this.durationMinutes = durationMinutes;
    }

    public String getDoctorName() {
        return doctorName;
    }

    public void setDoctorName(String doctorName) {
        this.doctorName = doctorName;
    }

    public AppointmentStatus getStatus() {
        return status;
    }

    public void setStatus(AppointmentStatus status) {
        this.status = status;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Appointment{");
        sb.append("id=").append(id);
        sb.append(", created=").append(created);
        sb.append(", dateTime=").append(dateTime);
        sb.append(", durationMinutes=").append(durationMinutes);
        sb.append(", doctorName='").append(doctorName).append('\'');
        sb.append(", status=").append(status);
        sb.append(", price=").append(price);
        sb.append('}');
        return sb.toString();
    }
}
