package org.drewwills.appointments.rest.domain;

public enum AppointmentStatus {

    /**
     * The appointment has been created in the system, but has not yet been booked.
     */
    Available,

    /**
     * The appointment has been booked by a user and is no longer available.
     */
    Booked

}
