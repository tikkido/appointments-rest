package org.drewwills.appointments.rest.domain;

import org.springframework.data.repository.CrudRepository;

import java.time.Instant;
import java.util.List;

public interface AppointmentRepository extends CrudRepository<Appointment,Long> {

    List<Appointment> findByDateTimeBetween(Instant fromDate, Instant toDate);

}
