# This image aims for maximum slimness
FROM openjdk:8-jdk-alpine

MAINTAINER drewwills

# This image wraps a Spring Boot executable jar (that bundles all its dependencies)
COPY build/libs/appointments-rest-*.jar appointments-rest.jar

# TCP traffic from this image is available on port 8080
EXPOSE 8080

# Launch the executable jar
ENTRYPOINT ["java", "-jar", "appointments-rest.jar"]
